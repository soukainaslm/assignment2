const balanceText = document.getElementById("balanceText");
const loanText = document.getElementById("loanText");
const payText = document.getElementById("payText");
const payBackButton = document.getElementById("payBackButton");
const laptopSelector = document.getElementById("laptop-select");
const laptopText = document.getElementById("laptop-features");
const laptopImage = document.getElementById("laptop-image");
const laptopDesc = document.getElementById("laptop-description");
const laptopName = document.getElementById("laptop-name");
const laptopPrice = document.getElementById("laptop-price");

let balance = 0;
let loanBalance = 0;
let payBalance = 0;
const laptops = [];
let first = true;
const images = [];
let haveBoughtComputer = true;

fetchComputers()

document.getElementById("loanButton").addEventListener("click", () => { 
    if (loanBalance === 0 && haveBoughtComputer) { 
        attemptedLoan = window.prompt("How much do you want to Loan?");
        if (Number(attemptedLoan) <= balance*2 && Number(attemptedLoan) > 0) { 
            loanBalance = attemptedLoan;
            balance = balance + Number(loanBalance);
            haveBoughtComputer = false;
            updateText();
        }
        else if (Number(attemptedLoan < 1)) {
            window.alert("Sorry, we cannot grant this loan. Please enter a value above 0.")
        }
        else {
            window.alert("Sorry, we cannot grant this loan. Please apply for a smaller amount.");
        }
    }
    else if (!haveBoughtComputer) {
        window.alert("Sorry, we cannot grant this loan. You can only get one loan per computer purchase.");
    }
    else {
        window.alert("Sorry, we cannot grant this loan. Please pay back your current loan before applying for a new one.");
    }
})

document.getElementById("bankButton").addEventListener("click", () => {  
    if (payBalance > 0) { 
        if (loanBalance > 0) { 
            const payback = Math.round(payBalance / 10);
            if (loanBalance < payback) {
                payback -= loanBalance;
                loanBalance = 0;
                payBalance += loanBalance;
            }
            else {
                payBalance -= payback;
                loanBalance -= payback;
            }
        }
        balance += payBalance;
        payBalance = 0;
        updateText();
    }
})

payBackButton.addEventListener("click", () => { 
    if (loanBalance > 0 && payBalance > 0) {
        if (payBalance > loanBalance) {
            payBalance -= loanBalance;
            loanBalance = 0;
            balance += payBalance;
            payBalance = 0;
        }
        else {
            loanBalance -= payBalance;
            payBalance = 0;
        }
        updateText();
    }
})

document.getElementById("workButton").addEventListener("click", () => { 
    payBalance += 100;
    updateText();
})

function fetchComputers() { /
    fetch("https://noroff-komputer-store-api.herokuapp.com/computers")
    .then(function(response) {
        return response.json()
    })
    .then(function(result) { 
        let i = 0;
        for (const cpu of result) {
            const computer = new Laptop(
                cpu.id, cpu.title,
                cpu.description,
                cpu.specs,
                cpu.price,
                cpu.stock,
                cpu.active,
                images[i]
            );
           
            if (i === 4) computer.image = "https://noroff-komputer-store-api.herokuapp.com/assets/images/5.png"
            else computer.image = "https://noroff-komputer-store-api.herokuapp.com/"+computer.image;
            let element = document.createElement("option");
            element.value = computer;
            element.innerHTML = cpu.title;
            laptopSelector.appendChild(element);
            laptops[i] = computer;
            i++;
        }
        laptopSelector.addEventListener("change", handleSelectorChange);
    })
    .catch(function(error) {
        console.error("We have a problem: " + error)
    })
}

const handleSelectorChange = e => { 
    const i = e.srcElement.selectedIndex;
    laptopText.innerHTML = "";
    if (i > 0) {
        cpu = laptops[i-1];
        for (const f of cpu.specs) {
            laptopText.innerHTML += f + "<br>";
        }
        laptopImage.src = cpu.image;
        laptopDesc.innerHTML = cpu.desc;
        laptopName.innerHTML = cpu.title;
        laptopPrice.innerHTML = cpu.price + " NOK";
        if (first) {
            document.getElementById("laptop-image").style.opacity = "100";
            document.getElementById("buy-button").style.opacity = "100";
            first = false;
        }
    }
    
}

document.getElementById("buy-button").addEventListener("click", () => { 
    const i = laptopSelector.selectedIndex;
    if (i != 0) {
        const cpu = laptops[i-1];
        if (cpu.price <= balance) {
            balance -= cpu.price;
            haveBoughtComputer = true;
            updateText();
            window.alert("Congratulations! You've bought the " + cpu.title);
        }
        else {
            window.alert("You can't affort this computer right now.");
        }
    }
})

function updateText() { 
    balanceText.innerHTML = "Balance: " + balance + " Kr";
    loanText.innerHTML = "Loan: "+ loanBalance + " Kr";
    payText.innerHTML = "Pay: "+ payBalance + " Kr";
    if (loanBalance > 0) {
        loanText.style.opacity = "100";
        payBackButton.style.opacity = "100";
    } else {
        loanText.style.opacity = "0";
        payBackButton.style.opacity = "0";
    }
}

class Laptop {
    constructor(id, title, desc, specs, price, stock, active, image) {
        this.id = id;
        this.title = title;
        this.desc = desc;
        this.specs = specs;
        this.price = price;
        this.stock = stock;
        this.active = active;
        this.image = image;
    }
}
